cmake_minimum_required(VERSION 2.8.3)
project(neuroracer_configserver_wrapper)

# Find catkin macros and libraries
find_package(catkin REQUIRED COMPONENTS
)

# Declare ROS messages, services and actions

# catkin specific configuration
catkin_package(
  CATKIN_DEPENDS
)

# Build

include_directories(
  ${catkin_INCLUDE_DIRS}
)
