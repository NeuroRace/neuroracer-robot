# General

As suggested, you can add some udev rules. [10-local.rules](10-local.rules) contains rules of type

```bash
ACTION=="add", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6001", SYMLINK+="my_uart"
```
and should be created in /etc/udev/rules.d/

## Check Device-Variables
For retrieving required attributes, eg. USB0 device, execute

```bash
udevadm info -a -p  $(udevadm info -q path -n /dev/ttyUSB0)
```
For ACM or differently connected devices, modify the command accordingly.

## Further Information
* [https://wiki.archlinux.org/index.php/udev](https://wiki.archlinux.org/index.php/udev)
* [http://www.reactivated.net/writing_udev_rules.html](http://www.reactivated.net/writing_udev_rules.html)
